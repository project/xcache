<?php

/**
 * Return data from the persistent cache.
 *
 * @param $key
 *   The cache ID of the data to retrieve.
 * @param $table
 *   The table $table to store the data in. Valid core values are 'cache_filter',
 *   'cache_menu', 'cache_page', or 'cache' for the default cache.
 */
function cache_get($key, $table = 'cache') {
  return xcache_handle_get($cid, $bin);
}

/**
 * Store data in the persistent cache.
 *
 * @param $cid
 *   The cache ID of the data to store.
 * @param $table
 *   The table $table to store the data in. Valid core values are 'cache_filter',
 *   'cache_menu', 'cache_page', or 'cache'.
 * @param $data
 *   The data to store in the cache. Complex data types must be serialized first.
 * @param $expire
 *   One of the following values:
 *   - CACHE_PERMANENT: Indicates that the item should never be removed unless
 *     explicitly told to using cache_clear_all() with a cache ID.
 *   - CACHE_TEMPORARY: Indicates that the item should be removed at the next
 *     general cache wipe.
 *   - A Unix timestamp: Indicates that the item should be kept at least until
 *     the given time, after which it behaves like CACHE_TEMPORARY.
 * @param $headers
 *   A string containing HTTP header information for cached pages.
 */
function cache_set($cid, $table = 'cache', $data, $expire = CACHE_PERMANENT, $headers = NULL) {
  // Create new cache object.
  $cache = new stdClass;
  $cache->cid = $cid;
  $cache->data = $data;
  $cache->created = time();
  $cache->expire = $expire;
  $cache->headers = $headers;
  
  if (!xcache_handle_set($cid, $cache, $expire, $table)) {
    watchdog('xcache', 'Unable to set into cache: ' . $cid);
  }
}

/**
 *
 * Expire data from the cache. If called without arguments, expirable
 * entries will be cleared from the cache_page table.
 *
 * @param $cid
 *   If set, the cache ID to delete. Otherwise, all cache entries that can
 *   expire are deleted.
 *
 * @param $table
 *   If set, the table $table to delete from. Mandatory
 *   argument if $cid is set.
 *
 * @param $wildcard
 *   If set to TRUE, the $cid is treated as a substring
 *   to match rather than a complete ID. The match is a right hand
 *   match. If '*' is given as $cid, the table $table will be emptied.
 */
function cache_clear_all($cid = NULL, $table = NULL, $wildcard = FALSE) {
  $table = empty($table) ? 'cache' : $table;
  if (empty($cid) || $cid == '*') {
    //TODO: handle flush situation
  }
  else {
    $full_key = xcache_get_key($cid, $table);
    xcache_unset($full_key);
  }
}

/**
 * Get key
 *
 * @param $key
 *   The key of the data to retrieve.
 * @param $bin
 *   The bin/silo to look in. 
 */
function xcache_get_key($key, $bin) {
  static $prefix;

  if (empty($prefix)) {
    $prefix = variable_get('xcache_key_prefix', '');
  }
  $full_key = ($prefix ? $prefix . '-' : '') . $bin . '-' . $key;

  return urlencode($full_key);
}

function xcache_handle_get($key, $bin) {
  $full_key = xcache_get_key($key, $bin);
  
  if (isset($full_key)) {
    if (!$return = xcache_get($full_key)) {
      watchdog('xcache', 'Failed to get key: ' . $full_key, WATCHDOG_ERROR);
      $return = FALSE;
    }
  }
  
  return unserialize($return);
}

/**
 * Set data into apc cache
 *
 * @param $key
 *   The key of the data to retrieve.
 * @param $bin
 *   The bin/silo to look in. 
 */
function xcache_handle_set($key, $data, $expire = CACHE_PERMENANT, $bin = 'cache') {  
  if (!empty($key) && !empty($data)) {
    $full_key = xcache_get_key($key, $bin);
    if (!xcache_set($full_key, serialize($value), $expire)) {
      watchdog('xcache', 'Failed to set key: ' . $full_key, WATCHDOG_ERROR);
      $return = FALSE;
    }
    else {
      $return = TRUE;
    }
  }
  
  return $return;
}